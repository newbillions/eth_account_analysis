from html_helper import get_html_by_url
from dateutil import parser

MAX_PAGE_LIMIT = 50
ACCOUNT_IS_FLIPPING_THRESHOLD = 0.0
# ACCOUNT_IS_FLIPPING algorithm:
# ACCOUNT_FLIPPING_SCORE: If it is FLIPPING, we add +1, else we -1. with the weights of (token_txs)/(Total_token_txs)
#    Note: ACCOUNT_FLIPPING_SCORE is basically within [1,-1], so I set ACCOUNT_IS_FLIPPING_THRESHOLD of 0.0

#   Let's a particular account has 3 token txs
#       1. ZRX: 2498 (FLIPPING)
#       2. KYC: 1    (HOLD)
#       3. RDN: 1    (HOLD)
# This acount is definetely FLIPPING.
# STAT: Total # of token_txs = 2500 (2498+1+1)

# For example,
# ACCOUNT_FLIPPING_SCORE = 1*(2498/2500) + (-1)*(1/2500) + (-1)*(1/2500) =  0.9984.
# so, return "FLIPPING" 0.9984 >= 0.0


def is_flipping_helper(soup):
    trs = soup.find("div",{"class":"table-responsive"}).findAll("tr")
    tx_arr = []
    for tr_index,tr in enumerate(trs):
        if tr_index != 0:
            # print("tr_index:{}".format(tr_index))
            tds = tr.findAll("td")
            for td_index,td in enumerate(tds):
                if td_index == 1:
                    timestamp = td.find("span")["title"]
                elif td_index == 3:
                    # type
                    tx_type = td.find("span").text
                    if "IN" in tx_type:
                        tx_type = "IN"
                elif td_index == 5:
                    quantity = float(td.text.replace(",",""))
                elif td_index == 6:
                    token_name = td.find("a").text.replace(" ","")
            tx_arr.append([timestamp,tx_type,quantity,token_name])
    return tx_arr

def check_number_of_page(soup):
    total_number_of_page = 1
    try:
        d_s = soup.findAll("b")
        total_number_of_page = int(d_s[1].text)
    except:
        pass
    return total_number_of_page

def is_flipping_at_this_token(txs):
    result = False
    if len(txs) == 0:
        return result

    # Sort to make sure we start with the first one
    txs.sort(key=lambda x: x[0])

    # Assume the first token tx is always IN
    prev_time = txs[0][0]

    for x in range(1,len(txs)):
        timestamp,tx_type,quantity = txs[x]
        if tx_type == "OUT":
            time_diff_days = (timestamp - prev_time).days
            if time_diff_days == 0:
                result = True
                break
        else:
            prev_time = timestamp
    return result

def is_flipping(account):
    url = "https://etherscan.io/tokentxns?a={}".format(account)
    print(url)
    soup = get_html_by_url(url)

    no_matching_exist = soup.find("font",{"color":"black"})
    if no_matching_exist is not None:
        return ("No Token txs",-1.0)

    # Get all the token txs
    txs = is_flipping_helper(soup)
    total_number_of_page = min(check_number_of_page(soup),MAX_PAGE_LIMIT)

    if total_number_of_page != 1:
        for x in range(2,total_number_of_page+1):
            tmp_url = url+"&p="+str(x)
            print(tmp_url)
            soup = get_html_by_url(tmp_url)
            tmp_txs = is_flipping_helper(soup)
            txs += tmp_txs



    token_tx_hist = dict()
    total_number_of_token_txs = 0
    for tx in txs:
        timestamp_s,tx_type,quantity,token_name = tx
        timestamp = parser.parse(timestamp_s)
        if token_name not in token_tx_hist:
            token_tx_hist[token_name] = [(timestamp,tx_type,quantity)]
        else:
            token_tx_hist[token_name].append((timestamp,tx_type,quantity))

        total_number_of_token_txs += 1

    total_number_of_token = len(token_tx_hist)
    account_flipping_score = 0.0

    for token in token_tx_hist:
        token_tx = token_tx_hist[token]
        flipped_token = is_flipping_at_this_token(token_tx)
        if flipped_token: account_flipping_score += len(token_tx) / total_number_of_token_txs
        else:  account_flipping_score -= len(token_tx) / total_number_of_token_txs
    # print(account_flipping_score)
    if account_flipping_score >= ACCOUNT_IS_FLIPPING_THRESHOLD:
        return ("Flipping",account_flipping_score)
    else:
        return ("Hold",account_flipping_score)

# TEST_CASE
def test():
    flip,score = is_flipping("0xead6be34ce315940264519f250d8160f369fa5cd")
    assert(flip == "Flipping")

    flip,score = is_flipping("0x3477f07b252ccef3bf1945328dcf5381c28f2d44")
    assert(flip == "Flipping")

    flip,score = is_flipping("0xbA111AF1e365510B71c6460b3b833447F73e759D")
    assert(flip == "Flipping")

    flip,score = is_flipping("0x89A8dD90B16d7CBA55ed185Ef077B4d5a49F1B9C")
    assert(flip == "Hold")

    flip,score = is_flipping("0x96f0aa4251eb879290d36ae975c57a59f2a5472f")
    assert(flip == "Flipping")

    flip,score = is_flipping("0x47e33e4CcA71006abd77E50bf6Ae087b93b19549")
    assert(flip == "Hold")

    flip,score = is_flipping("0xcca71809e8870afeb72c4720d0fe50d5c3230e05")
    assert(flip == "Hold")

    print("ALL TEST_CASE PASSED!")

if __name__ == "__main__":
    test()
