warning_color = '\033[95m'
end_color = '\033[0m'

def print_warning(message):
    print(warning_color+message+end_color)
