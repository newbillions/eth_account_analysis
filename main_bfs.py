import requests
from collections import OrderedDict
from print_helper import print_warning
from etherscan_token_balance import check_erc_balanced_of,pretty_print_token_balance
from web3 import Web3, HTTPProvider, IPCProvider
from tabulate import tabulate
import humanfriendly
import sys
from etherscan_internal_tx import get_internal_txs_for_account
from amazon_db import put_item,get_item,check_for_address_name
from flipping_address_name import is_flipping
from token_tx_helper import main_token_tx

web3 = Web3(HTTPProvider('https://mainnet.infura.io:8545'))

top_limits = 5

def find_top_related_accounts(tx_arrs,root_account,receipt_amount_out,receipt_amount_in,top_type='OUT',is_frequency=False):
    if not is_frequency:
        if top_type == "OUT":
            print("\n----------------------Most Ether Sent Accounts Top {} -------------------------\n".format(str(top_limits)))
        else:
            print("\n----------------------Most Ether Received  Accounts Top {} -------------------------\n".format(str(top_limits)))
    else:
        if top_type == 'OUT':
            print("\n----------------------Highest Frequency Accounts (Send) Top {} -------------------------\n".format(str(top_limits)))
        else:
            print("\n----------------------Highest Frequency Accounts (Receive) Top {} -------------------------\n".format(str(top_limits)))

    root_account = root_account.lower()

    # Sort receipt_amount by their ETH contribution
    if top_type == 'OUT':
        sorted_receipt_amount = OrderedDict(sorted(receipt_amount_out.items(), key=lambda x: x[1],reverse=True))
    elif top_type == 'IN':
        sorted_receipt_amount = OrderedDict(sorted(receipt_amount_in.items(), key=lambda x: x[1],reverse=True))

    index = 0
    print_data = []
    if is_frequency:
        headers = ["Address","Tx Freq","Address_name"]
    else:
        headers = ["Address","ETH Amount","Address_name"]
    for address,amount in sorted_receipt_amount.items():
        if index < top_limits:
            if address == root_account:
                continue
            address_name = check_for_address_name(address)

            if address_name != address:
                print_data.append([address,humanfriendly.format_number(amount),address_name])
            else:
                print_data.append([address,humanfriendly.format_number(amount),""])
            index += 1
    print(tabulate(print_data, headers=headers,tablefmt='fancy_grid'))

    if not is_frequency:
        if top_type == "OUT":
            print("\n**********************Total ETH Sent \033[1;31m{}\033[0m\n".format(str(receipt_amount_out[root_account])))
        else:
            print("\n**********************Total ETH Received \033[1;32m{}\033[0m\n".format(str(receipt_amount_in[root_account])))
    else:
        if top_type == 'OUT':
            print("\n**********************Total Send Txs Count \033[1;31m{}\033[0m\n".format(str(receipt_amount_out[root_account])))
        else:
            print("\n**********************Total Receive Txs Count \033[1;32m{}\033[0m\n".format(str(receipt_amount_in[root_account])))


def parse_eth_txs(account_address):
    url = "http://api.etherscan.io/api?module=account&action=txlist&address={}&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken".format(account_address)
    print(url)
    response = requests.get(url)
    data = response.json()
    results = data["result"]
    print(len(results))
    txs = []
    for result in results:
        if result["isError"] == "0":
            value = web3.fromWei(int(result["value"]),"ether")
            from_a = result["from"]
            to_a = result["to"]
            if to_a == "":
                to_a = result["contractAddress"] + " contractAddress"
            txs.append([from_a,to_a,value])
    return txs

def build_receipt_amount(txs,is_frequency):
    receipt_amount_out = dict()
    receipt_amount_in = dict()

    for tx in txs:
        from_address,to_address,quantity = tx
        if is_frequency:
            quantity = 1
        if to_address not in receipt_amount_out:
            receipt_amount_out[to_address] = quantity
        else:
            receipt_amount_out[to_address] += quantity

        if from_address not in receipt_amount_in:
            receipt_amount_in[from_address] = quantity
        else:
            receipt_amount_in[from_address] += quantity
    return (receipt_amount_out,receipt_amount_in)

def in_out_main(account_address):
    txs = parse_eth_txs(account_address) + get_internal_txs_for_account(account_address)

    eth_amount_receipt_amount_out, eth_amount_receipt_amount_in = build_receipt_amount(txs,False)
    freq_amount_receipt_amount_out, freq_amount_receipt_amount_in = build_receipt_amount(txs,True)

    find_top_related_accounts(txs,account_address,eth_amount_receipt_amount_out,eth_amount_receipt_amount_in,'OUT')
    find_top_related_accounts(txs,account_address,freq_amount_receipt_amount_out,freq_amount_receipt_amount_in,"OUT",is_frequency=True)
    find_top_related_accounts(txs,account_address,eth_amount_receipt_amount_out,eth_amount_receipt_amount_in,'IN')
    find_top_related_accounts(txs,account_address,freq_amount_receipt_amount_out,freq_amount_receipt_amount_in,"IN",is_frequency=True)
    return txs

import pagerank
import collections

def run_page_rank(txs):
    edgeWeights = collections.defaultdict(lambda: collections.Counter())
    for tx in txs:
        [from_a,to_a,amount] = tx
        amount = float(amount)
        edgeWeights[from_a][to_a] = amount
    # Apply PageRank to the weighted graph:
    accountProbabilities = pagerank.powerIteration(edgeWeights)
    accountProbabilities.sort_values(ascending=False,inplace=True)
    print("-----------------------Top 20 PageRank-score accounts-----------------------")
    print(accountProbabilities[:20])

def analyze_account(account):
    account = account.lower()
    db_name,flip,score = get_item(account)
    if db_name is not None:
        print_warning("\n\t\t\t"+account+"\t\t"+db_name+"\n")
    else:
        print_warning("\n\t\t\t"+account+"\t\t\n")
    try:
        (tokens,total_value) = check_erc_balanced_of(account)
        print_warning("\n\ntotal_token_value:{}. \t\t\t {} ETH".format(total_value,web3.fromWei(web3.eth.getBalance(account),"ether")))
        pretty_print_token_balance(tokens)
    except:
        pass
    txs = in_out_main(account)
    run_page_rank(txs)
    df = main_token_tx(account)
    # print(df)

if __name__ == "__main__":
    analyze_account(sys.argv[1])
