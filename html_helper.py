
try:
    # For Python 3.0 and later
    from urllib.request import build_opener
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import build_opener

import datetime
def parse_date(target_data):
    target_data = target_data.replace(minute=0, hour=0, second=0, microsecond=0)
    return target_data

def get_today():
    return parse_date(datetime.datetime.utcnow())

from bs4 import BeautifulSoup

def get_html_by_url(url):
    opener = build_opener()
    opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
    # print("get_html_by_url "+url)
    try:
        response = opener.open(url)
    except:
        return None
    html = response.read()
    soup = BeautifulSoup(html,"lxml")
    return soup

def find_token_tx_helper(url):
    soup = get_html_by_url(url)
    if soup is None:
        return []
    trs = soup.findAll("tr")
    tx_arr = []
    for tr_index,tr in enumerate(trs):
        if tr_index != 0:
            # print("tr_index:{}".format(tr_index))
            tds = tr.findAll("td")
            for td_index,td in enumerate(tds):
                if td_index == 2:
                    # from_Address
                    m_a = td.find("a")
                    if m_a is None:
                        m_a = td.find("span")
                    from_address = m_a.text
                    # print("from_address:{}".format(from_address))
                elif td_index == 3:
                    # type
                    tx_type = td.find("span").text
                    if "IN" in tx_type:
                        tx_type = "IN"
                    # print("tx_type:{}".format(tx_type))
                elif td_index == 4:
                    # to_Address
                    m_a = td.find("a")
                    if m_a is None:
                        m_a = td.find("span")
                    to_address = m_a.text
                    # print("to_address:{}".format(to_address))
                elif td_index == 5:
                    quantity = float(td.text)
                    # print("quantity:{}".format(quantity))
            from_address = from_address.lower()
            to_address = to_address.lower()
            tx_arr.append([from_address,tx_type,to_address,quantity])
    return tx_arr

def get_total_number_of_page(url):
    soup = get_html_by_url(url)
    try:
        d_s = soup.findAll("b")
        return int(d_s[1].text)
    except:
        return 1

def find_tx_given_token_contract_address_and_account_address(contract_address,account):
    url = "https://etherscan.io/token/generic-tokentxns2?contractAddress={}&a={}&mode=".format(contract_address,account)
    total_number_of_page = get_total_number_of_page(url)
    tx_arrs = []
    print("total_number_of_page:{} for {} of {} token".format(total_number_of_page,account,contract_address))
    for i in range(1,total_number_of_page+1):
        print(i)
        tx_arr = find_token_tx_helper("{}&p={}".format(url,i))
        tx_arrs = tx_arrs + tx_arr
    return tx_arrs


def check_if_address_name_exists(account):
    url = "https://etherscan.io/address/{}".format(account)
    soup = get_html_by_url(url)
    name = soup.find("font",{"title":"NameTag"})
    if name is not None:
        return name.text
    else:
        return ""
