from html_helper import get_html_by_url
# pip install tabulate
from tabulate import tabulate
import humanfriendly

def check_erc_balanced_of(account):
    base_url = "https://etherscan.io/address/"+account
    soup = get_html_by_url(base_url)
    ul = soup.find("ul",{"id":"balancelist"})
    if ul is None:
        return
    lis = ul.findAll("li")
    total_value = ""
    tokens = []
    for index,li in enumerate(lis):
        if index == len(lis)-1:
            #total
            total_value = li.find("b").find("span").text
        else:
            all_texts = li.find("a").findAll(text=True)
            if len(all_texts) == 2:
                [name,amount] = all_texts
                tokens.append([name,"N/A",amount.split()[0],0.0])
            else:
                [name,total_value,amount,price] = all_texts
                tokens.append([name,"$"+price.split()[1],amount.split()[0],float(total_value.split()[0][1:].replace(",",""))])
    tokens.sort(key=lambda x: x[3],reverse=True)
    return (tokens,total_value)

def pretty_print_token_balance(tokens):
    headers = ['Symbol','Price','Amount','Total']
    data = []
    for value in tokens:
        symbol,price,amount,total = value
        total = "$"+humanfriendly.format_number(total)
        data.append([symbol,price,amount,total])
    print(tabulate(data, headers=headers,tablefmt='fancy_grid'))
