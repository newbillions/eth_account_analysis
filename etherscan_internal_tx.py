import requests
from time import time
from web3 import Web3, HTTPProvider, IPCProvider
web3 = Web3(HTTPProvider('https://mainnet.infura.io:8545'))

unit = 1000

def get_internal_txs_for_account_helper(account,page):
    url = "https://api.etherscan.io/api?module=account&action=txlistinternal&address={}&startblock=0&endblock=99999999&page={}&offset={}&sort=asc&apikey=YourApiKeyToken".format(account,page,unit)
    print(url)
    response = requests.get(url)
    data = response.json()
    results = data["result"]
    txs = []
    for result in results:
        if result["isError"] == "0":
            value = web3.fromWei(int(result["value"]),"ether")
            from_a = result["from"]
            to_a = result["to"]
            if to_a == "":
                to_a = result["contractAddress"] + " contractAddress"
            txs.append([from_a,to_a,value])
    return txs

def get_internal_txs_for_account(address):
    start_page = 1
    all_txs = []
    txs = get_internal_txs_for_account_helper(address,start_page)
    all_txs = txs
    while len(txs) != 0:
        start_page += 1
        txs = get_internal_txs_for_account_helper(address,start_page)
        all_txs += txs
    return all_txs
