import sys
from amazon_db import put_item
import csv

csv_file = sys.argv[1]

m_csv = open(csv_file)
csvReader = csv.reader(m_csv)

counter = 0
duplicate_dict = dict()
for line in csvReader:
    counter += 1
    if counter == 1: continue
    [account_address,account_name,flipping_score,is_flipping] = line
    account_address = account_address.lower()
    if is_flipping == "": is_flipping = " "
    if flipping_score == "": flipping_score = " "
    print(line)
    if account_address not in duplicate_dict:
        duplicate_dict[account_address] = (1,account_name)
    else:
        old_c,old_name = duplicate_dict[account_address]
        duplicate_dict[account_address] = (old_c+1,old_name+account_name)
    put_item(account_address,account_name,is_flipping,flipping_score)

for account_address in duplicate_dict:
    old_c,old_name = duplicate_dict[account_address]
    if old_c > 1:
        print("{} {}".format(account_address,duplicate_dict[account_address]))
m_csv.close()
