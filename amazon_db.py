import boto3
from html_helper import check_if_address_name_exists

# Get the service resource.
dynamodb = boto3.resource('dynamodb')

table_name = "eth_accounts"
table = dynamodb.Table(table_name)

def create_table():
    # Create the DynamoDB table.
    table = dynamodb.create_table(
        TableName='eth_accounts',
        KeySchema=[
            {
                'AttributeName': 'account_address',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'account_name',
                'KeyType': 'RANGE'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'account_address',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'account_name',
                'AttributeType': 'S'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

    # Wait until the table exists.
    table.meta.client.get_waiter('table_exists').wait(TableName='eth_accounts')

    # Print out some data about the table.
    print(table.item_count)

def put_item(account_address,account_name,is_flipping,flipping_score):
    account_address = account_address.lower()
    table.put_item(
    Item={'account_address': account_address,'account_name':account_name,'is_flipping': is_flipping,"flipping_score":str(flipping_score)
    })

def get_item(account_address):
    response = table.get_item(Key={'account_address': account_address,})
    try:
        item = response['Item']
        if "account_name" in item:
            account_name = item["account_name"]
        else:
            account_name = "N/A"
        if "is_flipping" in item:
            is_flipping = item["is_flipping"]
        else:
            is_flipping = "N/A"
        if "flipping_score" in item:
            flipping_score = str(item["flipping_score"])
        else:
            flipping_score = "N/A"

        return (account_name,is_flipping,flipping_score)
    except:
        return (None,None,None)

def check_for_address_name(address):
    db_name,flip,score = get_item(address)
    if db_name is not None:
        return db_name
    else:
        account_name = check_if_address_name_exists(address)
        if account_name != "":
            # flip,score = is_flipping(address)
            flip,score = 1,1
            put_item(address,account_name,flip,score)
            return account_name
        else:
            return address
# put_item(table,"123","123")
# account_name = get_item(table,"123")
# print(account_name)
# investor_type = {
# "0x3078F22015436d621062f7CC8334774EB5685E97":"吓神",
# "0xf18023908a52d7f058d40277f947748ab9619ef1":"doc.ai pre-sale",
# "0x94133870506af5d0644f41a2ee62cc387b81135e":"Kyber-Presale",
# "0x00ECCb63Ec07b4344a56E8fF1EFfBA1395E5f056":"Pantera",
# "0x69EA6b31ef305d6b99bB2d4c9D99456fA108b02A":"FBG",
# "0xFBb1b73C4f0BDa4f67dcA266ce6Ef42f520fBB98":"Exchange (Bittrex)",
# "0xead6be34ce315940264519f250d8160f369fa5cd":"Poloniex",
# "0xe4abc54f5a6288b60c18b361442a151fc4911da6":"BTC9",
# "0x94d4a0de64654e534930dba01e26fa4e6cc72bc9":"HitBTC",
# "0x77ad7d9c2439566d7fa08d30ef2e76d115c930b6":"BTER",
# "0xb8fa4d57e9a183f12bc4571258f3a01478d0226c":"ZRX_bigger holder",
# "0xcca71809e8870afeb72c4720d0fe50d5c3230e05":"ZRX_good seller",
# "0x008CBA584D756a6C79Aed990De90A453EEf5c0d0":"Wancoin - Pre-sale",
# "0xe8ca0a887c5469aa20db873e96bec39dff63d18c":"DHVC - Professor",
# "0xF48716E2463C04aDd5237A82792913c5251e875c":"DHVC - Ren Si",
# "0xBF6AC42a910327F8734297240C8be6c85893c46f":"Guo Tianfang",
# "0x0083F902B47aE93e852708B170ed9D90Cf666e51":"Seamus",
# "0x412778579ba228D9DA410e8514AB599F31426A66":"Xiaohan",
# "0xbA111AF1e365510B71c6460b3b833447F73e759D":"Gambit Group",
# "0x9F390bcF4a2B007868ab46106DA7dc3adc5a4738":"Elon",
# "0xe477292f1b3268687a29376116b0ed27a9c76170":"HERO ICO",
# "0x8f0cC7844EdCBE997716c0790B05A2e9bA3401E0":"Yubo - Main account - 1",
# "0x0049FAB7f5dD1F26F057BD5d972Ffc6ba3c349Dd":"Yubo - Main account - 2",
# "0x00e16cEf0C88D9e912DAc2F19E0883732005779E":"Yubo - New",
# "0x6444b375F51eC5F11A89605B2405b5D6C070073d":"Yubo - Kyber",
# "0x89A8dD90B16d7CBA55ed185Ef077B4d5a49F1B9C":"Yubo - 01",
# "0xd245156a863b9b0EF8a702f3D0Ce4e291f003C5F":"Yubo - 02",
# "0xc9C4D93CEeAA201e6795c506e36aDfFCc0D256EA":"Yubo - 03",
# "0x11e4E2b148654B51f26f46A519B5E02f9FFbf29D":"Yubo - 04",
# "0x47e33e4CcA71006abd77E50bf6Ae087b93b19549":"Yubo - Mist",
# "0xeea5369afe487d61414452e1f5818b62fe05f28a":"Yubo - Binance",
# "0x002D197aB5a01B52eFb6414158eC71E478d2Ab38":"Safe Account - 1",
# }
# investor_type = {
# "0x25bea2156246ca97a8352e1cbe4b19dd34bb307f":"[RDN Top] Flipping(big whale): Gensis Guy",
# "0xe767c46329e37c7197c0c5b9200185e6a66b67f2":"[RDN Top] Flipping(big whale): LINK, ENG, MDA, TNT, CVC, OMG",
# "0x198ef1ec325a96cc354c7266a038be8b5c558f67":"[RDN Top] Buy and Hold(big whale)",
# "0x3078f22015436d621062f7cc8334774eb5685e97":"[RDN Top] Flipping(Small whale): SNT, Argon, ADX, QTUM, LINK",
# "0xc8f387eb8edac64efae018dfd10036bdbc059369":"[RDN Top] Flipping+hold",
# "0x96f0aa4251eb879290d36ae975c57a59f2a5472f":"[RDN Top] Flipping",
# "0x72b16dc0e5f85aa4bbfce81687ccc9d6871c2965":"[RDN Top] Buy and Hold",
# "0x3477f07b252ccef3bf1945328dcf5381c28f2d44":"[RDN Top] Only Loopring",
# "0x0019142ea15d5e963a4b75ac72a6f64b344a8abb":"[RDN Top] Buy and Hold",
# "0x2c840d94dcf091b87fd63db7eb0885d9ca4b5f79":"[RDN Top] Flipping",
# "0x2681e0ee432c53a1d2fff0636363a90abac97832":"[RDN Top] Buy and Hold",
# "0x22b84d5ffea8b801c0422afe752377a64aa738c2":"[RDN Top] Buy and Hold(Big whale)"
# }
# from address_name import is_flipping
# for account in investor_type:
#     print(account)
#     flip,score = is_flipping(account)
#     put_is_flipping(account,investor_type[account],flip,str(score))
